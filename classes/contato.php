<?php

require_once 'database.php';

class Contato
{
  private $conn;

  public function __construct()
  {
    $database = new Database();
    $db = $database->dbConnection();
    $this->conn = $db;
  }


  public function runQuery($sql)
  {
    $stmt = $this->conn->prepare($sql);
    return $stmt;
  }

  public function insert($tipo, $valor, $pessoa_id)
  {
    try {
      $stmt = $this->conn->prepare("INSERT INTO contato (tipo, valor,pessoa_id) VALUES(:tipo, :valor,:pessoa_id)");
      $stmt->bindparam(":tipo", $tipo);
      $stmt->bindparam(":valor", $valor);
      $stmt->bindparam(":pessoa_id", $pessoa_id);
      $stmt->execute();
      return $stmt;
    } catch (PDOException $e) {
      echo $e->getMessage();
    }
  }


  public function update($tipo, $valor, $pessoa_id, $id)
  {
    try {
      $stmt = $this->conn->prepare("UPDATE contato SET tipo = :tipo, valor = :valor,pessoa_id = :pessoa_id WHERE id = :id");
      $stmt->bindparam(":tipo", $tipo);
      $stmt->bindparam(":valor", $valor);
      $stmt->bindparam(":pessoa_id", $pessoa_id);
      $stmt->bindparam(":id", $id);
      $stmt->execute();
      return $stmt;
    } catch (PDOException $e) {
      echo $e->getMessage();
    }
  }

  public function delete($id)
  {
    try {
      $stmt = $this->conn->prepare("DELETE FROM contato WHERE id = :id");
      $stmt->bindparam(":id", $id);
      $stmt->execute();
      return $stmt;
    } catch (PDOException $e) {
      echo $e->getMessage();
    }
  }

  public function redirect($url)
  {
    header("Location: $url");
  }
}
?>