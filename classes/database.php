<?php
class Database
{
    private $host;
    private $dbName;
    private $user;
    private $pass;

    public function __construct()
    {
        $this->host = getenv('DB_HOST');
        $this->dbName = getenv('DB_NAME');
        $this->user = getenv('DB_USER');
        $this->pass = getenv('DB_PASS');
    }

    public $conn;
    public function dbConnection()
    {
        $this->conn = null;

        try {
            $this->conn = new PDO(
                "mysql:host=" . $this->host . ";dbname=" . $this->dbName, $this->user, $this->pass,
                array(
                    PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
                    1002 => "SET NAMES utf8"
                )
            );

            // Console log da informação de conexão
            error_log('Conexão estabelecida com sucesso: ' . $this->host . ', ' . $this->dbName);
        } catch (PDOException $exception) {
            echo "Connection error: " . $exception->getMessage();
        }
        return $this->conn;
    }
}

?>