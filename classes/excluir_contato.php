<?php

require_once 'classes/contato.php';

if (isset($_POST['idContato'])) {
    $idContato = $_POST['idContato'];

    $Contato = new Contato();
    $Contato->delete($idContato);

    echo "Contato excluído com sucesso.";
}
?>