<?php

require_once 'database.php';

class Pessoa
{
  private $conn;

  public function __construct()
  {
    $database = new Database();
    $db = $database->dbConnection();
    $this->conn = $db;
  }


  public function runQuery($sql)
  {
    $stmt = $this->conn->prepare($sql);
    return $stmt;
  }

  public function insert($nome, $cpf, $data_nascimento)
  {
    try {
      $stmt = $this->conn->prepare("INSERT INTO pessoa (nome, cpf,data_nascimento) VALUES(:nome, :cpf,:data_nascimento)");
      $stmt->bindparam(":nome", $nome);
      $stmt->bindparam(":cpf", $cpf);
      $stmt->bindparam(":data_nascimento", $data_nascimento);
      $stmt->execute();
      return $stmt;
    } catch (PDOException $e) {
      echo $e->getMessage();
    }
  }


  public function update($nome, $cpf, $data_nascimento, $id)
  {
    try {
      $stmt = $this->conn->prepare("UPDATE pessoa SET nome = :nome, cpf = :cpf,data_nascimento = :data_nascimento WHERE id = :id");
      $stmt->bindparam(":nome", $nome);
      $stmt->bindparam(":cpf", $cpf);
      $stmt->bindparam(":data_nascimento", $data_nascimento);
      $stmt->bindparam(":id", $id);
      $stmt->execute();
      return $stmt;
    } catch (PDOException $e) {
      echo $e->getMessage();
    }
  }

  public function delete($id)
  {
    try {
      $stmt = $this->conn->prepare("DELETE FROM pessoa WHERE id = :id");
      $stmt->bindparam(":id", $id);
      $stmt->execute();
      return $stmt;
    } catch (PDOException $e) {
      echo $e->getMessage();
    }
  }

  public function lastInsertId()
  {
    return $this->conn->lastInsertId();
  }

  public function redirect($url)
  {
    header("Location: $url");
  }
}
?>