CREATE TABLE `pessoa` (
  id int(11) NOT NULL AUTO_INCREMENT,
  nome varchar(100) NOT NULL,
  cpf varchar(14) NOT NULL,
  data_nascimento date NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY cpf_UNIQUE (cpf)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE `contato` (
    id int(11) NOT NULL AUTO_INCREMENT,
    tipo VARCHAR(50)NOT NULL,
    valor VARCHAR(100)NOT NULL,
  PRIMARY KEY (`id`),
    pessoa_id INT,
    FOREIGN KEY (pessoa_id) REFERENCES pessoa(id) ON DELETE CASCADE
);