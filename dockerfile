FROM php:apache

# Instala a extensão PDO MySQL
RUN docker-php-ext-install pdo_mysql

# Copia os arquivos da aplicação para o diretório do Apache
COPY . /var/www/html

# Expõe a porta 80 para acessar a aplicação
EXPOSE 80

# Comando para iniciar o servidor Apache
CMD ["apache2-foreground"]
