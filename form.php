<?php
// Show PHP errors
ini_set('display_errors', 1);
ini_set('display_startup_erros', 1);
error_reporting(E_ALL);

require_once 'classes/pessoa.php';
require_once 'classes/contato.php';

$Pessoa = new Pessoa();
$Contato = new Contato();

$dataAtual = new DateTime();
$intervalo = new DateInterval('P1Y');
$dataMaxima = $dataAtual->add($intervalo);
$dataMaximaFormatada = $dataMaxima->format('Y-m-d');




if (isset($_GET['edit_id'])) {
  $id = $_GET['edit_id'];
  $stmt = $Pessoa->runQuery("SELECT * FROM pessoa WHERE id=:id");
  $stmt->execute(array(":id" => $id));
  $pessoaRow = $stmt->fetch(PDO::FETCH_ASSOC);


  $stmt = $Contato->runQuery("SELECT * FROM contato WHERE pessoa_id=:id");
  $stmt->execute(array(":id" => $id));
  $contatos = $stmt->fetchAll(PDO::FETCH_ASSOC);
} else {
  $id = null;
  $pessoaRow = null;
  $contatos = array();
}

if (isset($_POST['btn_save'])) {
  $nome = strip_tags($_POST['nome']);
  $cpf = strip_tags($_POST['cpf']);
  $data_nascimento = strip_tags($_POST['data_nascimento']);

  try {
    if ($id != null) {
      if ($Pessoa->update($nome, $cpf, $data_nascimento, $id)) {
        $Pessoa->redirect('index.php?updated');
      }
    } else {
      if ($Pessoa->insert($nome, $cpf, $data_nascimento)) {
        $Pessoa->redirect('index.php?inserted');
      } else {
        $Pessoa->redirect('index.php?error');
      }
    }
  } catch (PDOException $e) {
    echo $e->getMessage();
  }
}

if (isset($_POST['btn_contato'])) {
  $idContato = strip_tags($_POST['id_contato']);
  $valor = strip_tags($_POST['valor_contato']);
  $tipo = strip_tags($_POST['tipo_contato']);

  $nome = strip_tags($_POST['nome']);
  $cpf = strip_tags($_POST['cpf']);
  $data_nascimento = strip_tags($_POST['data_nascimento']);

  if (empty($valor) || empty($tipo)) {
    echo "<script>alert('Preencha os campos antes');</script>";
  } else {
    try {
      if ($id != null && $idContato != null) {
        if ($Contato->update($tipo, $valor, $id, $idContato)) {
          $Contato->redirect('form.php?edit_id=' . $id);

          // 
        }
      } else if ($id != null && $idContato == null) {
        if ($Contato->insert($tipo, $valor, $id)) {
          $Contato->redirect('form.php?edit_id=' . $id);
          // 
        }
      } else if ($id == null && $idContato == null) {

        try {
          if ($Pessoa->insert($nome, $cpf, $data_nascimento)) {
            $lastInsertedId = $Pessoa->lastInsertId();

            if ($Contato->insert($tipo, $valor, $lastInsertedId)) {
              $Contato->redirect('form.php?edit_id=' . $lastInsertedId);
            }
          }
        } catch (PDOException $e) {
          echo $e->getMessage();
        }
      }
    } catch (PDOException $e) {
      echo $e->getMessage();
    }
  }
}
?>


<!doctype html>
<html lang="pt-br">

<head>
  <?php require_once 'includes/head.php'; ?>
  <script src="js/form.js"></script>
</head>

<body>
  <?php require_once 'includes/header.php'; ?>
  <div class="container-fluid">
    <div class="row">
      <?php require_once 'includes/sidebar.php'; ?>
      <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
        <h1 style="margin-top: 10px"></h1>
        <p>Campos Obrigatorios (*)</p>
        <form method="POST">

          <div class="form-group">
            <label for="id">ID</label>
            <input class="form-control" type="text" name="id" id="id" readonly
              value="<?php isset($pessoaRow['id']) ? print($pessoaRow['id']) : '' ?>">
          </div>

          <div class="form-group">
            <label for="nome">Nome *</label>
            <input class="form-control" type="text" name="nome" id="nome" placeholder="Nome Completo"
              value="<?php isset($pessoaRow['nome']) ? print($pessoaRow['nome']) : '' ?>" required>
          </div>

          <div class="form-group">
            <label for="cpf">CPF *</label>
            <input type="text" id="cpf" name="cpf" type="text" id="cpf" step="0.1" name="cpf" required
              onkeydown="javascript: fMasc( this, mCPF );" maxlength="14" class="form-control cpf-mask"
              placeholder="Ex.: 000.000.000-00" onblur="ValidaCPF();"
              value="<?php echo isset($pessoaRow['cpf']) ? $pessoaRow['cpf'] : '' ?>">
          </div>

          <div class="form-group">
            <label for="data_nascimento">Data Nascimento *</label>
            <input class="form-control" type="date" name="data_nascimento" max="<?php echo $dataMaximaFormatada; ?>"
              id="data_nascimento" placeholder="Data de Nascimento" required
              value="<?php isset($pessoaRow['data_nascimento']) ? print($pessoaRow['data_nascimento']) : '' ?>">
          </div>

          <div class="mt-6">
            <h5>Adicionar/Editar Contato</h5>
            <label for="tipo_contato">Tipo de Contato:</label>
            <select name="tipo_contato" id="tipo_contato" style="cursor:pointer;">
              <option value="">Selecione...</option>
              <option value="Telefone">Telefone</option>
              <option value="Email">Email</option>
              <option value="WhatsApp">WhatsApp</option>
              <option value="Outro">Outro</option>
            </select>

            <label for="contato">Contato:</label>
            <input type="text" name="valor_contato" id="valor_contato">
            <input type="hidden" name="id_contato" id="id_contato" value="">

            <input class="btn btn-sm btn-success mb-2" type="submit" name="btn_contato" value="Salvar">


            <section class="mt-2">
              <h6>Lista de Contatos:</h6>
              <?php if (!empty($contatos)): ?>
                <ul>
                  <?php foreach ($contatos as $contato): ?>

                    <?php if (is_array($contato) && isset($contato['tipo']) && isset($contato['valor'])): ?>
                      <li>
                        <?php echo $contato['tipo'] . ': ' . $contato['valor']; ?>
                        <a href="#" class="pl-6"
                          onclick="editarContato(<?php echo htmlspecialchars(json_encode($contato), ENT_QUOTES); ?>);">Editar</a>

                        <a href="#" class="ml-3" onclick="excluirContato(<?php echo $contato['id']; ?>);">Excluir</a>
                      </li>
                    <?php endif; ?>

                  <?php endforeach; ?>
                </ul>
              <?php else: ?>
                <p>Nenhum contato adicionado ainda.</p>
              <?php endif; ?>
            </section>
          </div>


          <section class="w-100 d-flex justify-content-around gap-6">
            <a class="btn btn-danger mb-2 " href="index.php">
              Cancelar
            </a>

            <input class="btn btn-primary mb-2" type="submit" name="btn_save" value="Salvar">
          </section>
        </form>
      </main>
    </div>
  </div>
  <?php require_once 'includes/footer.php'; ?>
</body>

</html>