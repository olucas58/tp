<script src="js/suportes.js"></script>

<nav class="col-md-2 d-none d-md-block bg-light sidebar">
    <div class="sidebar-sticky">
        <ul class="nav flex-column">
            <li class="nav-item hoover">
                <a class="nav-link " href="form.php">
                    <span data-feather="plus-circle"></span>
                    Adicionar Pessoa
                </a>

                <a class="nav-link " href="#" onclick="suportes()">
                    <span data-feather="cloud"></span>
                    Suportes balanceados
                </a>
            </li>
        </ul>
    </div>
</nav>