<?php
ini_set('display_errors', 1);
ini_set('display_startup_erros', 1);
error_reporting(E_ALL);

require_once 'classes/pessoa.php';

$objPessoa = new Pessoa();

if (isset($_GET['delete_id'])) {
  $id = $_GET['delete_id'];
  try {
    if ($id != null) {
      if ($objPessoa->delete($id)) {
        $objPessoa->redirect('index.php?deleted');
      }
    } else {
      var_dump($id);
    }
  } catch (PDOException $e) {
    echo $e->getMessage();
  }
}

?>


<!doctype html>
<html lang="pt-br">

<head>
  <?php require_once 'includes/head.php'; ?>
</head>

<body>
  <?php require_once 'includes/header.php'; ?>
  <div class="container-fluid">
    <div class="row">

      <?php require_once 'includes/sidebar.php'; ?>
      <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
        <section class="d-flex flex-row justify-content-between align-items-center">
          <h1 style="margin-top: 10px">Pessoas </h1>

          <div></div>

          <a class="d-block d-md-none text-success" href="form.php">
            <span data-feather="plus-circle"></span>
          </a>
        </section>
        <?php
        if (isset($_GET['updated'])) {
          echo '<div class="alert alert-info alert-dismissable fade show" role="alert">
                  <strong>Pessoa <strong> atualizada com sucesso.
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true"> &times; </span>
                    </button>
                  </div>';
        } else if (isset($_GET['deleted'])) {
          echo '<div class="alert alert-info alert-dismissable fade show" role="alert">
                  <strong>Pessoa <strong> deletada com sucesso.
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true"> &times; </span>
                    </button>
                  </div>';
        } else if (isset($_GET['inserted'])) {
          echo '<div class="alert alert-info alert-dismissable fade show" role="alert">
                 <strong>Pessoa <strong> inserida com sucesso.
                   <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                     <span aria-hidden="true"> &times; </span>
                   </button>
                 </div>';
        } else if (isset($_GET['error'])) {
          echo '<div class="alert alert-info alert-dismissable fade show" role="alert">
                 <strong>Erro! <strong> Erro na execução , tente novamente mais tarde .
                   <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                     <span aria-hidden="true"> &times; </span>
                   </button>
                 </div>';
        }
        ?>
        <div class="table-responsive">
          <table class="table table-striped table-sm">
            <thead>
              <tr>
                <th>#</th>
                <th>Nome </th>
                <th>CPF </th>
                <th>Data Nascimento </th>
                <th></th>
              </tr>
            </thead>
            <?php
            $query = "SELECT * FROM pessoa";
            $stmt = $objPessoa->runQuery($query);
            $stmt->execute();
            ?>
            <tbody>
              <?php if ($stmt->rowCount() > 0) {
                while ($pessoaRow = $stmt->fetch(PDO::FETCH_ASSOC)) {
                  ?>
                  <tr>
                    <td>
                      <?php print($pessoaRow['id']); ?>
                    </td>

                    <td>
                      <a href="form.php?edit_id=<?php print($pessoaRow['id']); ?>">
                        <?php print($pessoaRow['nome']); ?>
                      </a>
                    </td>

                    <td>
                      <?php print($pessoaRow['cpf']); ?>
                    </td>

                    <td>

                      <?php
                      $datetime = new DateTime($pessoaRow['data_nascimento']);

                      $formattedDate = $datetime->format('d/m/Y');

                      print($formattedDate); ?>
                    </td>

                    <td>
                      <a class="confirmation" href="index.php?delete_id=<?php print($pessoaRow['id']); ?>">
                        <span data-feather="trash"></span>
                      </a>
                    </td>
                  </tr>


                <?php }
              } ?>
            </tbody>
          </table>

        </div>


      </main>
    </div>
  </div>
  <?php require_once 'includes/footer.php'; ?>

  <script>
    $('.confirmation').on('click', function () {
      return confirm('Certeza que deseja excluir essa pessoa ?');
    });
  </script>
</body>

</html>