
function ValidaCPF() {
    var cpf = document.getElementById("cpf").value;
    cpf = cpf.replace(/\D/g, '');
    var result = true;
    if (cpf.toString().length != 11 || /^(\d)\1{10}$/.test(cpf)) { result = false; }


    [9, 10].forEach(function (j) {
      var soma = 0, r;
      cpf.split(/(?=)/).splice(0, j).forEach(function (e, i) {
        soma += parseInt(e) * ((j + 2) - (i + 1));
      });
      r = soma % 11;
      r = (r < 2) ? 0 : 11 - r;
      if (r != cpf.substring(j, j + 1)) result = false;
    });

    if (result == false) {
      document.getElementById("cpf").value = '';
      return alert('CPF Invalido !');
    }
  }

  function excluirContato(idContato) {
    if (confirm("Tem certeza de que deseja excluir este contato?")) {
      var xhttp = new XMLHttpRequest();
      xhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
          location.reload();
        }
      };
      xhttp.open("POST", "classes/excluir_contato.php", true);
      xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
      xhttp.send("idContato=" + idContato);
    }
  }

  function fMasc(objeto, mascara) {
    obj = objeto
    masc = mascara
    setTimeout("fMascEx()", 1)
  }

  function fMascEx() {
    obj.value = masc(obj.value)
  }

  function mCPF(cpf) {
    cpf = cpf.replace(/\D/g, "")
    cpf = cpf.replace(/(\d{3})(\d)/, "$1.$2")
    cpf = cpf.replace(/(\d{3})(\d)/, "$1.$2")
    cpf = cpf.replace(/(\d{3})(\d{1,2})$/, "$1-$2")
    return cpf
  }

  function editarContato(contato) {
    document.getElementById('id_contato').value = contato.id;
    document.getElementById('tipo_contato').value = contato.tipo;
    document.getElementById('valor_contato').value = contato.valor;
  }