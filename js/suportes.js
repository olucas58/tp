function suportes() {
  const string = prompt("Digite a sequência de colchetes:");

  let resposta = false;

  const chaves = [];
  const esquerdos = ["(", "{", "["];
  const direitos = [")", "}", "]"];

  for (let i = 0; i < string.length; i++) {
    const char = string[i];

    if (esquerdos.includes(char)) {
      chaves.push(char);
    } else if (direitos.includes(char)) {
      const aberto = chaves.pop();
      const fechado = direitos.indexOf(char);
      const igualAberto = esquerdos[fechado];

      if (aberto !== igualAberto) {
        resposta = false;
      }
    }
  }

  resposta = chaves.length === 0;

  if (resposta) {
    alert("A sequência de colchetes é válida.");
  } else {
    alert("A sequência de colchetes não é válida.");
  }
}
